#Grads In Games - Search For A Star 2018 - Molecumulate
## Introduction
While I have never participated in a game jam before, I have a somewhat familiar relationship with Unity as it is my de facto engine when learning about new game concepts. Because of this, I'm hoping to make good use of the knowledge I have acquired over the years without going overboard or attempting to do something out of my depth.
## Aims
My aims for this project are threefold:
- Improve what already exists.
    - Identify the features currently implemented into the game and see how they can be improved beyond their current iteration.
- Expand upon what already exists with new implementations.
    - Identify potential areas where existing features could be expanded and see if such expansions are necessary or not.
- Innovate a new feature not already present in the game.
    - Identify potential new features that could expand existing gameplay or open up possibilities for new gameplay elements.
## Standardisations
N/A
## Plan
### Stage 1 - Improve
- [x] Improve score GUI.
- [x] Fix logo distortion.
- [x] Change camera perspective.
- [x] Fix level abnormalities.
### Stage 2 - Innovate
- [x] Add time limit.
- [x] Implement more gameplay.
- [x] Create additional levels.
- [x] Create main menu and in-game menu.
## Progress
### 0 – Setup	(19/11/17)
Initial fork and setup.
### 1 – Clean-Up	(20/11/17)
I cleaned up the HUD by adding a background to the two scores in addition to adding a timer function which will put a limit on the amount of time a round can be played (currently 3 minutes). I also added a list of controls to the bottom corners of the screen for the respective players but I plan on replacing these with handy images instead so as to make it easier to understand.

I have also implemented a kick function which allows players to "kick" one another into the puddles of water. There still a few issues such as gravity getting confused after a player gets "kicked" but the basic concept is there for now. I have also modified the camera to zoom in and out depending on where the two players are at any given time and included panning left and right to keep the angled perspective.

![See Screenshot_1.png](Unity_Project/Screenshot_1.png)
### 2 - Making Changes	(3/12/17)
I made significant changes between the last commit and this one, these changes have been made to adapt to the current game premise; that the player is an atom collecting electrons by eliminating the other player. In accordance with this, the player design has been changed to a generic cube to represent an atom nucleus. The point system has also changed as the GUI contains a single electron orbiting each player's points which increases in speed with each point gained. A new cube is created to and orbits around the player as well to with each new point gained to represent an electron, with electrons being assigned to orbits of eight. The timer GUI has also changed to have an electron orbiting it with the minute and second count stacked to make it more visually appealing.

![See Screenshot_2_1.png](Unity_Project/Screenshot_2_1.png)

Another significant change I made was the change from water to a digital / atom style wave of cubes for the death trigger. This felt appropriate as it kept in line with the aesthetic theme that was starting to form. I'm going to look into having this wave aesthetic for all the level geometry with a lesser strength to prevent characters from being flung up into the air or having to jump every time just to move. This also gives the idea of "stable" and "unstable" geometry, with unstable geometry being what gives the other player points.

![See Screenshot_2_2.png](Unity_Project/Screenshot_2_2.png)
### 3 - Addition (23/1/18)
I made a new level with the wave-like effect (seen for the water in the previous screenshot) applied to the ground, I also made the water invisible until either player is close enough for aesthetic purposes. I've also added a hard mode which has the same effect for the ground in addition to a main menu with which to change the level and difficulty. The main menu title also has atoms orbiting it in all three dimensions with the atoms orbiting the players having their orbits changed to match this pattern. This, however, causes the electron paths to look like waving tentacles, as a result I'll continue to work on the formula until I have a result I'm satisfied with.

![See Screenshot_3_1.png](Unity_Project/Screenshot_3_1.png)
![See Screenshot_3_2.png](Unity_Project/Screenshot_3_2.png)
![See Screenshot_3_3.png](Unity_Project/Screenshot_3_3.png)

My plan for the next update is to create a few more levels, improve the electron orbit paths and create an end of game screen for the timer.
### 4 - Final Submission (28/1/18)
I now have a total of three levels each with two difficulties making for a total of six unique playing experiences. I have not adjusted the electron orbit paths as I did not have time to go back and rework it within the allotted time; I was, however, able to implement an end screen which displays the winning player or a draw screen if there isn't one before returning to the main menu scene. I have also removed the "kick" function as it wasn't fully functional, instead opting for a spinning tops style collision to nudge the other player into the wave of cubes. With regards to this, the player's collision box increases every eight points / electrons to make it easier for the winning player to knock the other player while also giving them the disadvantage of being a larger target. Finally I added a simplistic instructions image to the top of the screen and moved the Search For A Star logo to the main menu scene.

![See Screenshot_4_1.png](Unity_Project/Screenshot_4_1.png)

All in all, I am satisfied with the final product.