﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuOrbit3D : MonoBehaviour
{
    public Material electronMaterial;
    private List<GameObject> electronList = new List<GameObject>();

    // Use this for initialization
    void Start ()
    {
        for (int i = 0; i < 8; i++)
        {
            electronList.Add(GameObject.CreatePrimitive(PrimitiveType.Cube));
            electronList[i].transform.SetParent(this.transform);
            electronList[i].transform.localRotation = Quaternion.Euler(0, 0, 0);
            electronList[i].GetComponent<Renderer>().material = electronMaterial;
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        for (int i = 0; i < electronList.Count; i++)
        {
            float orbitX = Mathf.Cos((Time.realtimeSinceStartup - (i * Mathf.PI / 8)) * 2) * 0.5f * 15;  //cos(time - (1/8 orbit)) * 2) / 2
            float orbitY = (i % 2 == 0) ? Mathf.Cos((Time.realtimeSinceStartup - (i * Mathf.PI / 8)) * 2) * 0.5f * 5 : Mathf.Sin((Time.realtimeSinceStartup - (i * Mathf.PI / 8)) * 2) * 0.5f * 5;  //Mathf.Cos OR Mathf.Sin((Time.realtimeSinceStartup - (i * 2.25f)) * 2) * 0.5f;
            float orbitZ = Mathf.Sin((Time.realtimeSinceStartup - (i * Mathf.PI / 8)) * 2) * 0.5f * 5;  //sin(time - (1/8 orbit)) * 2) / 2
            electronList[i].transform.localPosition = new Vector3(orbitX, orbitY, -orbitZ);
        }
    }
}