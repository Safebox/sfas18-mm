﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ElectronOrbit3D : MonoBehaviour
{
    public UIManager scoreController;
    public Material electronMaterial;
    private List<GameObject> electronList = new List<GameObject>();
    
    void Update()
    {
        if (this.name.Contains("1"))
        {
            if (electronList.Count < scoreController.player1Score)
            {
                electronList.Add(GameObject.CreatePrimitive(PrimitiveType.Cube));
                electronList[electronList.Count - 1].transform.localScale = new Vector3(0.25f, 0.25f, 0.25f);
                electronList[electronList.Count - 1].transform.SetParent(this.transform);
                electronList[electronList.Count - 1].transform.localRotation = Quaternion.Euler(0, 0, 0);
                electronList[electronList.Count - 1].GetComponent<Renderer>().material = electronMaterial;
                Destroy(electronList[electronList.Count - 1].GetComponent<BoxCollider>());
            }
            
            this.GetComponent<CharacterController>().radius = 0.5f + (0.5f * (scoreController.player1Score / 8));
        }
        else
        {
            if (electronList.Count < scoreController.player2Score)
            {
                electronList.Add(GameObject.CreatePrimitive(PrimitiveType.Cube));
                electronList[electronList.Count - 1].transform.localScale = new Vector3(0.25f, 0.25f, 0.25f);
                electronList[electronList.Count - 1].transform.SetParent(this.transform);
                electronList[electronList.Count - 1].transform.localRotation = Quaternion.Euler(0, 0, 0);
                electronList[electronList.Count - 1].GetComponent<Renderer>().material = electronMaterial;
                Destroy(electronList[electronList.Count - 1].GetComponent<BoxCollider>());
            }
            
            this.GetComponent<CharacterController>().radius = 0.5f + (0.5f * (scoreController.player2Score / 8));
        }

        for (int i = 0; i < electronList.Count; i++)
        {
            float orbitX = Mathf.Cos((Time.realtimeSinceStartup - (i * Mathf.PI / 8)) * 2) * 0.5f * ((i + 8) / 8); //cos(time - (1/8 orbit)) * 2) / 2 * orbital path number (eight atoms per orbital path)
            float orbitY = (i % 2 == 0) ? Mathf.Cos((Time.realtimeSinceStartup - (i * Mathf.PI / 8)) * 2) * 0.5f * ((i + 8) / 8) : Mathf.Sin((Time.realtimeSinceStartup - (i * Mathf.PI / 8)) * 2) * 0.5f * ((i + 8) / 8);  //Mathf.Cos OR Mathf.Sin((Time.realtimeSinceStartup - (i * 2.25f)) * 2) * 0.5f;
            float orbitZ = Mathf.Sin((Time.realtimeSinceStartup - (i * Mathf.PI / 8)) * 2) * 0.5f * ((i + 8) / 8); //sin(time - (1/8 orbit)) * 2) / 2 * orbital path number (eight atoms per orbital path)
            electronList[i].transform.localPosition = new Vector3(orbitX, orbitY, -orbitZ);
        }
    }
}
