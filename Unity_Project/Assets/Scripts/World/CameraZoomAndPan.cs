﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoomAndPan : MonoBehaviour
{
    public Transform player1;
    public Transform player2;

    private float defaultFOV;

    void Start()
    {
        defaultFOV = this.GetComponent<Camera>().fieldOfView;
    }

    void Update()
    {
        this.GetComponent<Camera>().fieldOfView = Mathf.Lerp(20, defaultFOV, Vector3.Distance(player1.position, player2.position) / 20f);
        //mid.position = new Vector3((player1.position.x + player2.position.x) / 2, (player1.position.y + player2.position.y) / 2, (player1.position.z + player2.position.z) / 2);
        this.transform.LookAt((player1.position + player2.position) / 2);
        this.transform.position = new Vector3((player1.position.x + player2.position.x) / 2, this.transform.position.y, this.transform.position.z);
    }
}