﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public struct LevelSelector
    {
        public string sceneName;
        public string displayName;

        public LevelSelector(string sceneName, string displayName)
        {
            this.sceneName = sceneName;
            this.displayName = displayName;
        }
    }

    public LevelSelector[] scenes = {
        new LevelSelector("IslandScene", "The Island"),
        new LevelSelector("SteppingStonesScene", "Stepping Stones"),
        new LevelSelector("RingsScene", "Two Rings")
    };
    public int curIndex = 0;
    public Text[] menuItems;
    public Color selectedColor;
    public Color unselectedColor;

    public void HighlightMenuItem(int i)
    {
        for (int j = 0; j < menuItems.Length; j++)
        {
            if (i == j)
            {
                menuItems[j].color = selectedColor;
            }
            else
            {
                menuItems[j].color = unselectedColor;
            }
        }
    }

    public void Play()
    {
        if (curIndex % 2f != 0)
        {
            PlayerPrefs.SetInt("global_hardMode", 1);
        }
        else
        {
            PlayerPrefs.SetInt("global_hardMode", 0);
        }
        UnityEngine.SceneManagement.SceneManager.LoadScene(scenes[Mathf.FloorToInt(curIndex / 2f) % scenes.Length].sceneName);
    }

    public void SwitchLevel(Text caller)
    {
        /*curIndex++;
        curIndex = Mathf.FloorToInt(curIndex % (scenes.Length * 2f));
        caller.text = "<--" + ((curIndex / 2f != 0f) ? scenes[curIndex / scenes.Length].displayName + "-->\n(Hard Mode)" : scenes[curIndex / scenes.Length].displayName + "-->\n(Easy Mode)");*/
        
        curIndex++;
        caller.text = "<--" + ((curIndex % 2f != 0f) ? scenes[Mathf.FloorToInt(curIndex / 2f) % scenes.Length].displayName + "-->\n(Hard Mode)" : scenes[Mathf.FloorToInt(curIndex / 2f) % scenes.Length].displayName + "-->\n(Easy Mode)");

        Debug.Log(curIndex + ", " + (curIndex / 2f) + ", " + (Mathf.FloorToInt(curIndex / 2f) % scenes.Length));
    }

    public void Quit()
    {
        PlayerPrefs.DeleteKey("global_hardMode");
        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        #endif
        Application.Quit();
    }
}