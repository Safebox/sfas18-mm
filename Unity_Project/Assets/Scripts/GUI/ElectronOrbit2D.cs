﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ElectronOrbit2D : MonoBehaviour
{
    public UIManager scoreController;

    void Update()
    {
        if (this.name.Contains("1"))
        {
            this.GetComponent<RectTransform>().localPosition = new Vector3(Mathf.Cos(Time.realtimeSinceStartup * scoreController.player1Score) * 64, Mathf.Sin(-Time.realtimeSinceStartup * scoreController.player1Score) * 64, 0);
        }
        else
        {
            this.GetComponent<RectTransform>().localPosition = new Vector3(Mathf.Cos(Time.realtimeSinceStartup * scoreController.player2Score) * 64, Mathf.Sin(-Time.realtimeSinceStartup * scoreController.player2Score) * 64, 0);
        }
    }
}
