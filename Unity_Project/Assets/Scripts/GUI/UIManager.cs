﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    // --------------------------------------------------------------

    [SerializeField]
    Text m_Player1ScoreText;

    [SerializeField]
    Text m_Player2ScoreText;

    //-----
    [SerializeField]
    Text minuteText;
    [SerializeField]
    Text secondText;
    [SerializeField]
    Color player1winColour;
    [SerializeField]
    Color player2winColour;
    //-----

    // --------------------------------------------------------------

    int m_Player1Score = 0;
    int m_Player2Score = 0;

    //-----
    public int player1Score
    {
        get
        {
            return m_Player1Score;
        }
        private set
        {
            m_Player1Score = value;
        }
    }

    public int player2Score
    {
        get
        {
            return m_Player2Score;
        }
        private set
        {
            m_Player2Score = value;
        }
    }
    //-----

    // --------------------------------------------------------------

    void OnEnable()
    {
        DeathTrigger.OnPlayerDeath += OnUpdateScore;
    }

    void OnDisable()
    {
        DeathTrigger.OnPlayerDeath -= OnUpdateScore;
    }

    void OnUpdateScore(int playerNum)
    {
        if (playerNum == 1)
        {
            m_Player2Score += 1;
            m_Player2ScoreText.text = "" + m_Player2Score;
        }
        else if (playerNum == 2)
        {
            m_Player1Score += 1;
            m_Player1ScoreText.text = "" + m_Player1Score;
        }
    }

    //-----
    void Update()
    {
        //Countdown Timer
        int tempMinute = ((60 * 3) - Mathf.RoundToInt(Time.timeSinceLevelLoad)) / 60;
        int tempSecond = ((60 * 3) - Mathf.RoundToInt(Time.timeSinceLevelLoad)) % 60;

        //End Screen
        if (tempMinute == 0 && tempSecond < 0)
        {
            tempMinute = 0;
            tempSecond = 0;

            this.transform.Find("EndScreen").gameObject.SetActive(true);
            if (player1Score > player2Score)
            {
                this.transform.Find("EndScreen").gameObject.GetComponent<Text>().text = "Player 1 Wins!";
                this.transform.Find("EndScreen").gameObject.GetComponent<Text>().color = player1winColour;
            }
            else if (player2Score > player1Score)
            {
                this.transform.Find("EndScreen").gameObject.GetComponent<Text>().text = "Player 2 Wins!";
                this.transform.Find("EndScreen").gameObject.GetComponent<Text>().color = player2winColour;
            }
            else
            {
                this.transform.Find("EndScreen").gameObject.GetComponent<Text>().text = "Draw!";
            }
            
            if (((60 * 3) - Mathf.RoundToInt(Time.timeSinceLevelLoad)) % 60 == -3)
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenuScene");
            }
        }

        minuteText.text = tempMinute.ToString();
        secondText.text = tempSecond.ToString().PadLeft(2,'0');

        //Countdown Electron
        minuteText.transform.GetChild(1).GetComponent<RectTransform>().localPosition = new Vector3(Mathf.Cos((60 * 3) - Time.timeSinceLevelLoad) * 48, (Mathf.Sin((60 * 3) - Time.timeSinceLevelLoad) * -48) - 12, 0);
    }
    //-----
}
