﻿using System.Collections;

using System.Collections.Generic;
using UnityEngine;

public class AtomicWaterController : MonoBehaviour
{
    public float waveSpeed = 5;
    public float waveMagnitude = 0.5f;
    public Vector2 waveDimensions;
    public Material waveMaterial;
    internal GameObject[,] waveCubes;
    internal GameObject waveCollider;

    public Transform player1;
    public Transform player2;

    internal bool hardMode;

    public virtual void Start()
    {
        waveCubes = new GameObject[(int)waveDimensions.x, (int)waveDimensions.y];
        for (int x = 0; x < waveCubes.GetLength(0); x++)
        {
            for (int y = 0; y < waveCubes.GetLength(1); y++)
            {
                waveCubes[x, y] = GameObject.CreatePrimitive(PrimitiveType.Cube);
                waveCubes[x, y].transform.SetParent(this.transform);
                waveCubes[x, y].GetComponent<MeshRenderer>().material = waveMaterial;
            }
        }
        hardMode = (PlayerPrefs.GetInt("global_hardMode") == 1) ? true : false;
    }

    public virtual void Update()
    {
        for (int x = 0; x < waveCubes.GetLength(0); x++)
        {
            for (int y = 0; y < waveCubes.GetLength(1); y++)
            {
                waveCubes[x, y].transform.localPosition = new Vector3(x + 0.5f, Mathf.Lerp(Mathf.Cos(Time.realtimeSinceStartup - waveCubes[x,y].transform.position.x - 0.5f), Mathf.Sin(Time.realtimeSinceStartup * waveSpeed - waveCubes[x, y].transform.position.z - 0.5f), 0.5f) * waveMagnitude, y + 0.5f);
                if (hardMode)
                {
                    float newScale = Mathf.Clamp01(3f - Mathf.Min(Vector3.Distance(waveCubes[x, y].transform.position, player1.position), Vector3.Distance(waveCubes[x, y].transform.position, player2.position)));
                    waveCubes[x, y].transform.localScale = new Vector3(newScale, newScale, newScale);
                }
            }
        }
    }
}