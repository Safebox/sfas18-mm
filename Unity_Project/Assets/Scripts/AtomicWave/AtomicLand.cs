﻿using System.Collections;

using System.Collections.Generic;
using UnityEngine;

public class AtomicLand : AtomicWaterController
{
    public override void Start()
    {
        base.Start();

        waveCollider = new GameObject();
        waveCollider.transform.SetParent(this.transform);
        waveCollider.transform.localPosition = Vector3.zero;
        waveCollider.AddComponent<BoxCollider>();
        waveCollider.GetComponent<BoxCollider>().center = new Vector3(waveDimensions.x / 2, 0, waveDimensions.y / 2);
        waveCollider.GetComponent<BoxCollider>().size = new Vector3(waveDimensions.x, 1, waveDimensions.y);
    }

    public override void Update()
    {
        base.Update();
    }
}