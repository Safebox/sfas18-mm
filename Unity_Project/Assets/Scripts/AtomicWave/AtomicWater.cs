﻿using System.Collections;

using System.Collections.Generic;
using UnityEngine;

public class AtomicWater : AtomicWaterController
{
    public override void Start()
    {
        base.Start();

        for (int x = 0; x < waveCubes.GetLength(0); x++)
        {
            for (int y = 0; y < waveCubes.GetLength(1); y++)
            {
                Destroy(waveCubes[x, y].GetComponent<BoxCollider>());
            }
        }
        waveCollider = new GameObject();
        waveCollider.transform.SetParent(this.transform);
        waveCollider.transform.localPosition = Vector3.zero;
        waveCollider.AddComponent<BoxCollider>();
        waveCollider.GetComponent<BoxCollider>().center = new Vector3(waveDimensions.x / 2, 0, waveDimensions.y / 2);
        waveCollider.GetComponent<BoxCollider>().size = new Vector3(waveDimensions.x, 1, waveDimensions.y);
        waveCollider.GetComponent<BoxCollider>().isTrigger = true;
        waveCollider.AddComponent<DeathTrigger>();
        hardMode = true;
    }

    public override void Update()
    {
        base.Update();
    }
}